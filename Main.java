public class Main {
	private static boolean debug=false;		/*depura el codigo*/
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double num[]={	
				Math.random()*1000000000000000L,
				Math.random()*1000000000000000000L,
				Math.random()*1000000000000000000L,
				Math.random()*1000000000L,
				Math.random()*1000000000000000000L,
				Math.random()*1000000000000000000L,
				Math.random()*1000000000000000000L,
				Math.random()*10000L,
				Math.random()*1000000000000000000L,
				Math.random()*1000000000000000000L	};
		for (int gir=0;gir<num.length;gir++) {
			System.out.println( "b)"+String.format("%021.2f:", num[gir])+num2text(num[gir]) );
		}
	}
	///////////////////////////////////////////////////
	public static String num2text(double numero) {
	////////////////////////////////////////////////////
		/**
		 * TODO:	Convierte un double a texto limitado a 18digitos, valores mayores omite
		 */
		if (numero>1E18) {
			return "Error overflow";
		}
		int gir,pos;
		boolean ingroup,special=false;
		String strtext	="",	entero	="",segmento	="",strnum	="";
		String vec1[]	={"Once","Doce","Trece","Catorce","Quince","Dieciseis","Diecisiete","Dieciocho","Diecinueve"};
		String vec2[][]	={	{"Un",		"Diez",			"Ciento"		},
							{"Dos",		"Veinte",		"Doscientos"	},
							{"Tres",	"Treinta",		"Trescientos"	},
							{"Cuatro",	"Cuarenta",		"Cuatrocientos"	},
							{"Cinco",	"Cincuenta",	"Quinientos"	},
							{"Seis",	"Sesenta",		"Seiscientos"	},
							{"Siete",	"Setenta",		"Setecientos"	},
							{"Ocho",	"Ochenta",		"Ochocientos"	},
							{"Nueve",	"Noventa",		"Novecientos"	} };
		String grupo[]	={"Mil","Billones","Mil","Millones","Mil",""};
		strnum	=String.format("%021.2f",numero);
		// inicio casos especiales
		if (strnum.substring(0,18).contentEquals("000000000000000001")) {
			strtext=" Un";
			special=true;
		}
		if (strnum.substring(0,18).contentEquals("000000000000001000")) {
			strtext=" Mil";
			special=true;
		}
		if (strnum.substring(0,18).contentEquals("000000000001000000")) {
			strtext=" Un Millon";
			special=true;
		}
		if (strnum.substring(0,18).contentEquals("000000001000000000")) {
			strtext=" Mil Millones";
			special=true;
		}
		if (strnum.substring(0,18).contentEquals("000001000000000000")) {
			strtext=" Un Billones";
			special=true;
		}
		if (strnum.substring(0,18).contentEquals("001000000000000000")) {
			strtext=" Mil Billones";
			special=true;
		}
		
		// fin casos especiales
		if (!special) {
			entero=strnum.substring(0,18);
			debug("Convert:"+entero+" len:"+entero.length());
			gir=0;
			pos=0;
			while (gir<=5) {		/*123:000@000:000@000:000*/
				segmento=entero.substring(pos,pos+3);
				ingroup	=false;
				debug("gir:"+gir+", pos:"+pos+", segmento:"+segmento+", grupo:"+grupo[gir]);
				
				// casos especiales
				if (segmento.contentEquals("100")) {
					debug("case 1");
					strtext=strtext.concat(" Cien "+grupo[gir]);
					gir++;
					pos+=3;
					continue;
				}
				if (segmento.contentEquals("001")) {
					debug("case 2");
					strtext=strtext.concat(" Un "+grupo[gir]);
					gir++;
					pos+=3;
					continue;
				}
				
				// analizamos centena
				if (segmento.charAt(0)!='0') {
					debug("case 3");
					strtext=strtext.concat( " "+vec2[(int)segmento.charAt(0)-49][2] );
					ingroup=true;
				}
				
				// analizamos decena, caso especial 011,012,013...
				if (segmento.charAt(1)=='1' && segmento.charAt(2)!='0') {
					debug("case 4");
					strtext=strtext.concat( " "+vec1[(int)segmento.charAt(2)-49]+" " );
					strtext=strtext.concat( grupo[gir] );
					gir++;
					pos+=3;
					continue;
				}
				if (segmento.charAt(1)!='0') {
					debug("case 5");
					strtext=strtext.concat( " "+vec2[(int)segmento.charAt(1)-49][1] );
					ingroup=true;
				}
				
				// analizamos unidad
				if (segmento.charAt(2)!='0') {
					debug("case 6");
					strtext=strtext.concat( (segmento.charAt(0)!='0' || segmento.charAt(1)!='0') ? " y " : " "  );
					strtext=strtext.concat( vec2[(int)segmento.charAt(2)-49][0] );
					ingroup=true;
				}
				// agregamos texto de grupo
				if (ingroup) {
					debug("case 7");
					if (grupo[gir].length()>0) {
						strtext=strtext.concat(" ");
					}
					strtext=strtext.concat( grupo[gir] );
				}
				gir++;
				pos+=3;
			}	/*while*/
		}
		
		// procesa decimales ((luego de la coma))
		segmento=strnum.substring(19,21);
		debug("remain:"+segmento);
		if ( !segmento.contentEquals("00") ) {
			strtext	=strtext.concat(" con");
			debug("seg:"+segmento);
			// si decena es 11,12,13,14...
			if (segmento.charAt(0)=='1' && segmento.charAt(1)!='0') {
				strtext=strtext.concat( " "+vec1[(int)segmento.charAt(1)-49] );
			}
			// si decena es diferente a 0
			if (segmento.charAt(0)!='0') {
				strtext=strtext.concat( " "+vec2[(int)segmento.charAt(0)-49][1] );
				if (segmento.charAt(1)!='0') {
					strtext=strtext.concat( " y "+vec2[(int)segmento.charAt(1)-49][0] );
				}
			}
			// si decena es 0
			if (segmento.charAt(0)=='0') {
				if (segmento.charAt(1)!='0') {
					strtext=strtext.concat( " "+vec2[(int)segmento.charAt(1)-49][0] );
				}
			}
			strtext=strtext.concat(" cts");
		}
		return strtext;
	}
	////////////////////////////////////////////
	public static void debug( String str ) {
	////////////////////////////////////////////
		/**
		 * TODO:	Depura el punto de ejecucion si esta habilitado
		 */
		if (debug)
			System.out.println(str);
	}
}

